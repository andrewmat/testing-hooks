import React from 'react'
import Carousel from './examples/CarouselExample'
import './App.scss'

const App = () => {
  return (
    <div>
      <Carousel />
    </div>
  )
}

export default App
